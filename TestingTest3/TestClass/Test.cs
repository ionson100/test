﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection.Emit;
using test3;

namespace TestingTest3
{
    [Serializable]
    class Test : ISerializeTranslate<Color>, ISerializeTranslate<TypeBuilder>
    {

        public Color Color { get; set; }

        string ISerializeTranslate<Color>.Serialize(Color t)
        {
            var str = ColorTranslator.ToHtml(t);
            return str;
        }

        Color ISerializeTranslate<Color>.Deserialize(string s)
        {
            return string.IsNullOrWhiteSpace(s) ? Color.Empty : Color.FromName(s);
        }


        public Hashtable dd = new Hashtable { { 2323, 344444 }, { 34344, 45 } };
        private Queue _queue = new Queue();
        private Queue _queue1;
        public string[] Dstring = { "dssd\"", "dfsdfsdf" };
        public char[] Dchar = { '\"', '5' };
        public char[] Dchar1 = { (char)56, (char)34 };
        Dictionary<int, string> _dictionary = new Dictionary<int, string> { { 1, "45" } };
        public Queue Queue
        {
            get { return _queue; }
            set { _queue = value; }
        }
        private int? ff = 34;

        public string Serialize(TypeBuilder t)
        {
            throw new NotImplementedException();
        }

        TypeBuilder ISerializeTranslate<TypeBuilder>.Deserialize(string s)
        {
            throw new NotImplementedException();
        }
    }
}