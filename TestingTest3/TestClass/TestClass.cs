﻿using System;
using System.Collections.Generic;
using System.Drawing;
using test3;

namespace TestingTest3
{
    [Serializable]
    public class TestClass : ISerializeTranslate<Color>
    {


        string ISerializeTranslate<Color>.Serialize(Color t)
        {
            var str = ColorTranslator.ToHtml(t);
            return str;
        }

        Color ISerializeTranslate<Color>.Deserialize(string s)
        {
            return string.IsNullOrWhiteSpace(s) ? Color.Empty : Color.FromName(s);
        }


        public Color ColorBase;

        [NonSerialized]
        public int? int1;
        private byte? byte1 = null;
        private bool? isbool1 = null;
        EnumTest _enumTest = EnumTest.First;
        Assa assa = new Assa();
        public  Dictionary<object, object> DictionaryO = new Dictionary<object, object> { { new List<object>() { 2, 3, 4 }, new Assa() { ssrrrrrrrrrr = "sdsadad" } }, { "e", 2 } };//.ToArray();
        Dictionary<object, List<object>> f2 = new Dictionary<object, List<object>> { { 1, new List<object> { 2, 4 } } };
        private Guid _rGuidf = Guid.NewGuid();
        public char C = 'd';
        private byte df = 34;
        private bool isbool = true;
        public string Name = "fsdfsdfdd";
        private DateTime f3 = DateTime.Now;
        private DateTime[] _list3 = { new DateTime().ToLocalTime() };
        private object[] _list34 = { "dfdf", new Assa(), new AssaClass() };
        private string[] _list2 = { "1", "3" };
        public List<TestClass> List2 = new List<TestClass>();
        HashSet<int> _hashSet = new HashSet<int> { 2, 3, 4, 5 };
        Dictionary<string, int> myObj = new Dictionary<string, int> { { "q", 2 }, { "e", 2 } };
        public int er = 456;
        private string Name2;
        private List<object> _list = new List<object> { 1, 3, 4, 5, 6, 7, 8, 9, 230 };
        private object[] _list4 = { "dfdf", new Assa(), new AssaClass() };
        [NonSerialized]
        private int fd;
        public object[] dstring = { "dssd\"", new Test() };
        public char[] dchar = { '\"', '5' };
        public char[] dchar1 = { (char)44, (char)34 };
        Dictionary<int, string> _dictionary = new Dictionary<int, string>() { { 1, "45" } };
        public Int32 Age
        {
            get
            {
                return fd;
            }
            set
            {
                fd = value;
            }
        }
        public TestClass TestClassfClass;
        public Assa Assa;
        public object sdobj = new AssaClass();
        public TimeSpan Tspan;
        public string Serialize(TimeSpan t)
        {
            return t.ToString();
        }
        public string Name100 { get; set; }




    }
    [Serializable]
    public struct Assa
    {

        public string ssrrrrrrrrrr;
        public int we;

    }
    [Serializable]
    public class AssaClass
    {

        public string ssff;
        public int we;

    }
    [Serializable]
    public enum EnumTest
    {
        First = 0, Last = 1
    }
    [Serializable]
    public class TestInterfaceSerializeTranslate : ISerializeTranslate<String>, ISerializeTranslate<int>
    {
        public string Name { get; set; }
        public string Serialize(string t)
        {
            return "ttt" + t + "ffff";
        }

        public string Deserialize(string s)
        {
            return s;
        }

        public int Index { get; set; }

        string ISerializeTranslate<int>.Serialize(int t)
        {
            return "100";
        }

        int ISerializeTranslate<int>.Deserialize(string s)
        {
            return 100;
        }
    }

    interface ITestSer<T>
    {
        T Name { get; set; }
    }

    [Serializable]
    class TestClassInterface : ITestSer<TestClass>
    {

        public TestClass Name
        {
            get
                ;
            set
                ;
        }
    }
}
