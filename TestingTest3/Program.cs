﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using test3;


namespace TestingTest3
{
    partial class Program
    {
        private const int Iteration = 3;
        static void Main()
        {
            Test(new Test());// { Dstring = new[] { "dsdsd" } });
            //выполнять под релизом, в дебаге пишет в файл


            var sw = new Stopwatch();
            sw.Start();
            TestBinaryFormatter();
            sw.Stop();
            var t1 = sw.ElapsedTicks;

            var sw1 = new Stopwatch();
            sw1.Start();
            TestCore();
            sw1.Stop();
            var t11 = sw1.ElapsedTicks;



            Console.WriteLine("test3-{0}   BinaryFormatter-{1}", t11, t1);




            var f = new TestClass();
            for (int i = 0; i < Iteration; i++)
            {
                f.List2.Add(new TestClass
                {
                    Age = 343434,
                    Tspan = new TimeSpan(34355),
                    Name = "sadasd",
                    dchar = new[] { '3', 'r', '5' },
                    int1 = null,
                    dstring = new object[] { "sasdasd\"x", 3456 }
                });
                f.DictionaryO.Add(i, "weqwe" + i);
            }
            Test1(f);

            int result0;
            using (var fs = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(fs, f);
                result0 = fs.ToArray().Count();
            }


            var result1 = Serialize.SerializeCore(f);
            var resTest = Serialize.DeserializeCore<TestClass>(result1);

            Console.WriteLine("Сериализация предложенным вариантом объекта типа TestClass");
            Console.WriteLine("объект -> Json->компрессия");
            Console.WriteLine("размер массива байт: " + result1.Count());
            Console.WriteLine("сериализация BinaryFormatter: " + result0);



            Console.ReadKey();

        }



        static void Test1<T>(T t)
        {
            byte[] ree;
            T res;
            using (var fs = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(fs, t);
                ree = fs.ToArray();
            }
            using (var fds = new MemoryStream(ree))
            {
                var bf = new BinaryFormatter();
                res = (T)bf.Deserialize(fds);
            }
            Compare(t, res);
        }

        static void Test<T>(T t)
        {
            // var res = Serialize.DeserializeFromJson<T>(Serialize.SerializeToJson(t));//без сжатия
            var res = Serialize.DeserializeCore<T>(Serialize.SerializeCore(t));//с сжатием
            Compare(t, res);
        }



        static void Compare(object o1, object o2)
        {
            if (o1 == null && o2 == null)
            {
                return;
            }
            if (o1.GetType() != o2.GetType())
            {
                throw new SerializationException("d");
            }



            if (o1.GetType().IsNumeric() || o1.GetType().IsNullable() || o1 is bool || o1.GetType().IsEnum || o1 is DateTime || o1 is Guid || o1 is char || o1 is TimeSpan || o1 is Color)
            {
                if (o1.ToString() != o2.ToString())
                {
                    throw new Exception("d");
                }
                return;
            }
            if (o1 is string)
            {
                if (!string.Equals(o1, o2))
                {
                    throw new Exception("d");
                }
                return;
            }

            if (o1.GetType().IsDictionary())
            {
                var k1 = ((IDictionary)o1).Keys;
                var k2 = ((IDictionary)o2).Keys;

                var v1 = ((IDictionary)o1).Values;
                var v2 = ((IDictionary)o2).Values;



                if (k1.Count != k2.Count)
                {
                    throw new Exception("d");
                }
                var l1 = k1.CollectionToList<object>();
                var l2 = k2.CollectionToList<object>();
                for (int i = 0; i < k1.Count; i++)
                {
                    Compare(l1[i], l2[i]);
                }
                var lv1 = v1.CollectionToList<object>();
                var lv2 = v2.CollectionToList<object>();
                for (int i = 0; i < k1.Count; i++)
                {
                    Compare(lv1[i], lv2[i]);
                }
                return;

            }

            if (o1.GetType().GetInterfaces().Contains(typeof(IEnumerable)))
            {
                var v1 = ((IEnumerable)o1).Cast<object>().ToList();
                var v2 = ((IEnumerable)o2).Cast<object>().ToList();
                if (v1.Count != v2.Count)
                {
                    throw new Exception("d");
                }
                for (int i = 0; i < v1.Count; i++)
                {
                    Compare(v1[i], v2[i]);
                }
                return;
            }

            var field = o1.GetType().GetFields(test3.Helper.BindingFlags).Where(a => a.IsNotSerialized == false).ToList();
            {
                foreach (var fieldInfo in field)
                {
                    var f1 = o1.GetType().GetField(fieldInfo.Name, test3.Helper.BindingFlags);
                    var v1 = f1.GetValue(o1);
                    var info = o2.GetType().GetField(fieldInfo.Name, test3.Helper.BindingFlags);

                    var v2 = info.GetValue(o2);
                    Compare(v1, v2);

                }
            }




        }


    }
}
