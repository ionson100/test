﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TestingTest3
{
    static class Helper
    {
        public static List<T> CollectionToList<T>(this ICollection other)
        {
            var output = new List<T>(other.Count);

            output.AddRange(other.Cast<T>());

            return output;
        }
    }
}