﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Script.Serialization;


namespace TestingTest3
{
    partial class Program
    {


        static void TestCore()
        {
            Test(new Test());
            var @interface = new TestClassInterface();
            ITestSer<TestClass> ser = @interface;
            ser.Name = new TestClass();
            var tt = new Test();
            //tt.Queue.Enqueue("3");
            //tt.Queue.Enqueue(new TestClass());
            //tt.Color = Color.Black;
            Queue queue = new Queue();
            queue.Enqueue(234);
            char d = 'Y';
            var serializer = new JavaScriptSerializer();
            var ee = serializer.Serialize(d);
            var dd = new ArrayList { new Test() };
         

            var f = new TestClass();
            for (int i = 0; i < Iteration; i++)
            {
                f.List2.Add(new TestClass()
                            {
                                Age = 343434,
                                Tspan = new TimeSpan(34355),
                                Name = "sadasd",
                                dchar = new[] { '3', 'r', '5' },
                                int1 = null,
                                dstring = new object[] { "sasdasd\"x", 3456 }
                            });
            }
            Test(f);
            Test(new Dictionary<int, string> { { 1, "45" } });
            Test(new Test());
            Test(d);
            Test("ASDASD\"DFDFSDFDF");
            Test(dd);
            Test(new HashSet<int>(new[] { 1, 2, 3, 4 }));
            Test(new Hashtable { { 2323, new TestClass() }, { 34344, 45 } });
            Test(tt);
            Test(queue);
            Test(ser);
            Test(DateTime.Now);
            Test("fsdfdfsf");
            Test(new object[] { "dfdf", new Assa(), new AssaClass() });
            Test<bool?>(true);
            Test((int?)434);
            Test((int?)434);
            Test(EnumTest.First);
            Test(new[] { new DateTime().ToLocalTime() });
            Test(new HashSet<int> { 2, 3, 4, 5 });
            Test(new object[] { "dfdf", new Assa(), new AssaClass() });
            Test(new Dictionary<object, object> { { "q", 2 }, { "e", 2 } });
            Test(new List<int> { 1, 3, 4, 5, 5, 5, 6 });
            Test(new[] { "1", "3" });
            Test(new List<object> { 2, 3, 4, 5 });
            Test(Guid.NewGuid());
            Test((char)100);
            Test(Guid.NewGuid());
            Test((byte)34);
            Test(345345);
            Test(true);
            Test(Guid.NewGuid());
            Test(new[] { 1, 3, 4 });
            Test(new Dictionary<string, object> { { "q", 2 }, { "e", 2 } });
            Test(new Assa { ssrrrrrrrrrr = "erwewer", we = 56456 });
            Test(new Dictionary<object, object> { { new List<object> { 2, 3, 4 }, new Assa { ssrrrrrrrrrr = "sdsadad" } }, { "e", 2 } });
        }

        static void TestBinaryFormatter()
        {
            Test1(new Test());
            var @interface = new TestClassInterface();
            ITestSer<TestClass> ser = @interface;
            ser.Name = new TestClass();
            var tt = new Test();
            //tt.Queue.Enqueue("3");
            //tt.Queue.Enqueue(new TestClass());
            //tt.Color = Color.Black;
            var queue = new Queue();
            queue.Enqueue(234);
            char d = 'Y';
            var serializer = new JavaScriptSerializer();
            var ee = serializer.Serialize(d);
            var dd = new ArrayList { new Test() };
          

            var f = new TestClass();
            for (int i = 0; i < Iteration; i++)
            {
                f.List2.Add(new TestClass
                            {
                                Age = 343434,
                                Tspan = new TimeSpan(34355),
                                Name = "sadasd",
                                dchar = new[] { '3', 'r', '5' },
                                int1 = null,
                                dstring = new object[] { "sasdasd\"x", 3456 }
                            });
            }
            Test1(f);
            Test1(new Dictionary<int, string> { { 1, "45" } });
            Test1(new Test());
            Test1(d);
            Test1("ASDASD\"DFDFSDFDF");
            Test1(dd);
            Test1(new HashSet<int>(new[] { 1, 2, 3, 4 }));
            Test1(new Hashtable { { 2323, new TestClass() }, { 34344, 45 } });
            Test1(tt);
            Test1(queue);
            Test1(ser);
            Test1(DateTime.Now);
            Test1("fsdfdfsf");
            Test1(new object[] { "dfdf", new Assa(), new AssaClass() });
            Test1<bool?>(true);
            Test1((int?)434);
            Test1((int?)434);
            Test1(EnumTest.First);
            Test1(new[] { new DateTime().ToLocalTime() });
            Test1(new HashSet<int> { 2, 3, 4, 5 });
            Test1(new object[] { "dfdf", new Assa(), new AssaClass() });
            Test1(new Dictionary<object, object> { { "q", 2 }, { "e", 2 } });
            Test1(new List<int> { 1, 3, 4, 5, 5, 5, 6 });
            Test1(new[] { "1", "3" });
            Test1(new List<object> { 2, 3, 4, 5 });
            Test1(Guid.NewGuid());
            Test1((char)100);
            Test1(Guid.NewGuid());
            Test1((byte)34);
            Test1(345345);
            Test1(true);
            Test1(Guid.NewGuid());
            Test1(new[] { 1, 3, 4 });
            Test1(new Dictionary<string, object> { { "q", 2 }, { "e", 2 } });
            Test1(new Assa { ssrrrrrrrrrr = "erwewer", we = 56456 });
            Test1(new Dictionary<object, object> { { new List<object> { 2, 3, 4 }, new Assa { ssrrrrrrrrrr = "sdsadad" } }, { "e", 2 } });
        }
    }
}
