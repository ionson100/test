﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace test3
{
    internal static class LazyBoy
    {
       


        public static readonly Func<Type, bool> IsListLazy =
             type => type.IsGenericType && type.GetInterfaces().Contains(typeof(IList));

        public static readonly Func<Type, bool> IsDictionaryLazy =
            type => type.IsGenericType && type.GetInterfaces().Contains(typeof(IDictionary));


        public static readonly Func<Type, bool> IsSetLazy =
            type => type.IsGenericType && type == typeof(ISet<>).MakeGenericType(type).GetGenericArguments()[0];

        public static readonly Func<Type, bool> IsNullableLazy =
            type => type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);

        public static readonly Func<Type, bool> IsNumericLazy = type =>
                                                                {
                                                                    while (true)
                                                                    {
                                                                        switch (Type.GetTypeCode(type))
                                                                        {
                                                                            case TypeCode.Byte:
                                                                            case TypeCode.Decimal:
                                                                            case TypeCode.Double:
                                                                            case TypeCode.Int16:
                                                                            case TypeCode.Int32:
                                                                            case TypeCode.Int64:
                                                                            case TypeCode.SByte:
                                                                            case TypeCode.Single:
                                                                            case TypeCode.UInt16:
                                                                            case TypeCode.UInt32:
                                                                            case TypeCode.UInt64:
                                                                                return true;
                                                                            case TypeCode.Object:
                                                                                if (!type.IsGenericType || 
                                                                                    type.GetGenericTypeDefinition()
                                                                                    != typeof(Nullable<>)) return false;
                                                                                type = Nullable.GetUnderlyingType(type);
                                                                                continue;
                                                                        }
                                                                        return false;
                                                                    }

                                                                };

     

        public static readonly Func<object, string, object> GetValueFieldLazy =
           (obj, s) => obj.GetType().GetField(s).GetValue(obj);

        public static readonly Func<FieldInfo, object, object> GetValueCoreLazy =
             (info, o) => info.GetValue(o);

        public static readonly Action<FieldInfo, object, object> SetValueCoreLazy =
            (fieldInfo, target, value) => fieldInfo.SetValue(target, value);

        public static readonly Func<Type, FieldInfo, Type> TypeISerializeTranslateLazy =
          (type, fieldInfo) =>
          {
              var tt = type.GetInterfaces().Where(a => a.IsGenericType && a.GetGenericTypeDefinition() == typeof(ISerializeTranslate<>));
              var enumerable = tt as Type[] ?? tt.ToArray();
              return enumerable.SingleOrDefault(a => a.GetGenericArguments().First() == fieldInfo.FieldType);
          };

        public static readonly Func<Type, MethodInfo> GetMethodParse =
            (type) => type.GetMethod("Parse", new[] {typeof (String)});

        public static readonly Func<Type, bool> IsEnumerableLazy =
            type => type.GetInterfaces().Contains(typeof (IEnumerable));

        public static readonly Func<Type, bool> IsCollectionLazy = type => type.GetInterfaces()
            .Contains(typeof (ICollection));


       

    }


}