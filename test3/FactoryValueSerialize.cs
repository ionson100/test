﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace test3
{
    internal static class FactoryValueSerialize
    {
        public static string GetValue(FieldInfo fieldInfo, object obj, Workflow utils)
        {
            var res = fieldInfo.GetValueCore(obj);
            var fieldType = fieldInfo.FieldType;
            if (fieldType == typeof(string) || fieldType == typeof(char))
            {
                return res == null ? ConstString.NULL : string.Format("\"{0}\"", res.ToString().Replace("\"", "\\\""));
            }

            if (fieldType.IsEnum)
            {
                return string.Format("\"{0}\"", fieldInfo.GetValueCore(obj));
            }

            if (fieldType.IsNumeric())
            {
                return res == null ? ConstString.NULL : res.ToString();
            }
            if (fieldType.GetMethodParse() != null)//&& fieldInfo.FieldType.IsSerializable
            {
                return res == null ? ConstString.NULL : string.Format("\"{0}\"", res);
            }
            if (fieldType.IsPrimitive == false && fieldType.IsValueType)
            {
                return utils.SeralizeCore(fieldInfo.GetValueCore(obj), fieldType == typeof(object));
            }

            if (fieldType.IsClass)
            {
                var rese = utils.SeralizeCore(fieldInfo.GetValueCore(obj), fieldType == typeof(object));
                return obj == null ? ConstString.NULL : rese;
            }

            throw new SerializationException("Не обработаное исключение..");
        }

        public static string GetSingleValue(object o, StringBuilder are, Type fieldInfo, bool isObject, Workflow utils)
        {
            if (isObject)
            {
                return utils.SeralizeCore(o, true);
            }

            if (o is string || o is char)
            {
                return string.Format("\"{0}\"", o.ToString().Replace("\"", "\\\""));
            }

            if (o.GetType().IsNumeric())
            {
                return string.Format("{0}", o);
            }

            if (o.GetType().GetMethodParse() != null)//&& fieldInfo.IsSerializable
            {
                return string.Format("\"{0}\"", o);
            }

            return utils.SeralizeCore(o, fieldInfo == typeof(Object));
        }


        internal static void SerializeEnumerator(Type fieldType, StringBuilder sb, string last, Workflow workflow, object values, string fielInfoName)
        {
            var are = new StringBuilder();

            if (values == null)
            {
                are.Append(ConstString.NULL);
            }
            else
            {

                foreach (var o in (IEnumerable)values)
                {

                    if (fieldType.IsDictionary())
                    {
                        var r1 = o.GetType().GetGenericArguments();
                        if (!r1[0].IsNumeric() && r1[0] != typeof(string))
                        {
                            var ee = Helper.DictionaryToArray((dynamic)values);
                            are.AppendFormat("\"{2}{0}\":{1},", fielInfoName, workflow.SeralizeCore(ee, false), ConstString.Dictionary);
                            continue;
                        }

                        {
                            var key = o.GetType().GetMethod("get_Key").Invoke(o, null);
                            var value = o.GetType().GetMethod("get_Value").Invoke(o, null);
                            if (key.GetType().IsNumeric())
                            {

                            }
                            are.AppendFormat("{2}{0}{2}:{1},", GetSingleValue(key, are, fieldType, r1[0] == typeof(object), workflow),
                                GetSingleValue(value, are, fieldType, r1[1] == typeof(object), workflow), key.GetType().IsNumeric() ? "\"" : string.Empty);
                            continue;
                        }
                    }

                    if (fieldType.IsList() || fieldType.IsSet() || fieldType.IsCollection())
                    {
                        if (fieldType.IsGenericType)
                        {
                            are.AppendFormat("{0},", GetSingleValue(o, are, fieldType, fieldType.GetGenericArguments().First() == typeof(object), workflow));
                        }
                        else
                        {
                            are.AppendFormat("{0},", GetSingleValue(o, are, fieldType, true, workflow));
                        }
                        continue;
                    }
                    if (fieldType.IsArray)
                    {
                        are.AppendFormat("{0},", GetSingleValue(o, are, fieldType, fieldType == typeof(object[]), workflow));
                    }



                }
            }

            var ogr1 = values != null ? "[" : "";
            var ogr2 = values != null ? "]" : "";
            sb.AppendFormat("\"{0}\":{3}{1}{4}{2}", fielInfoName, are.ToString().TrimEnd(','),
                last, fieldType.IsDictionary() ? "{" : ogr1, fieldType.IsDictionary() ? "}" : ogr2);
        }




        internal static object GetValueParse(object obj, Type types)
        {
            if (obj == null) return null;
            var type = types;
            if (types.IsGenericType && types.IsNullable())
                type = types.GetGenericArguments().First();
          
            if (types.IsEnum) 
                return Enum.Parse(types, obj.ToString());
        
            if (type == typeof (string)) 
                return obj.ToString();

            if (types.IsArray)
                type = types.GetElementType();
           


            var parse = type.GetMethodParse();
          var res=  parse.Invoke(obj, new object[] {obj.ToString()});
            return res;

        }
    }
}