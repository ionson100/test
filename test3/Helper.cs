﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace test3
{

    public static class Helper
    {
        public static T ToEnumerable<T,TD>(this ArrayList list , T t,TD d)
        {
        
            return  (T) list.Cast<T>();
        }

    

        //internal static readonly Func<TransmitterAsynchronousMethod, string> AsyncMethodParsePartJsom =
        //   Workflow.GedPartJson;
        // new Lazy<Func<Func<Workflow , string , Type , FieldInfo , object ,PartJson>>>(() => Workflow.GedPartJson, LazyBoy.Sd);

        // internal delegate string AsyncMethodParsePartJsom(Workflow workflow, uint idPart, string last, Type typer, FieldInfo fieldInfo, object obj);
        public static int MaxJsonLength = 1000000000;

        public static BindingFlags BindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic |
                                           BindingFlags.Static;
        #region Ex


        public static MethodInfo GetMethodParse(this Type type)
        {

            return LazyBoy.GetMethodParse(type);
        }


        public static object TypeSerializeTraslate(this Type info, FieldInfo fieldInfo)
        {

            return LazyBoy.TypeISerializeTranslateLazy.Invoke(info, fieldInfo);
        }


        public static object GetValueCore(this FieldInfo info, object obj)
        {

            return LazyBoy.GetValueCoreLazy.Invoke(info, obj);
        }

        public static void SetValueCore(this FieldInfo info, object target, object value)
        {

            LazyBoy.SetValueCoreLazy.Invoke(info, target, value);
        }


        public static object GetValueField(this Type type, object obj, string fielName)
        {

            return LazyBoy.GetValueFieldLazy.Invoke(obj, fielName);
        }

        public static bool IsList(this Type type)
        {

            return LazyBoy.IsListLazy.Invoke(type);
        }

        public static bool IsDictionary(this Type type)
        {
            return LazyBoy.IsDictionaryLazy.Invoke(type);
        }

        public static bool IsSet(this Type type)
        {
            return LazyBoy.IsSetLazy.Invoke(type);
        }

        public static bool IsNullable(this Type type)
        {
            return LazyBoy.IsNullableLazy.Invoke(type);
        }

        public static bool IsNumeric(this Type type)
        {

            return LazyBoy.IsNumericLazy.Invoke(type);
        }

       

        public static bool IsEnumerable(this Type type)
        {
            return LazyBoy.IsEnumerableLazy.Invoke(type);
        }

        public static bool IsCollection(this Type type)
        {
            return LazyBoy.IsCollectionLazy.Invoke(type);
        }


        #endregion

        internal static byte[] Compression(string s)
        {

            var byteArray = new byte[0];
            if (string.IsNullOrEmpty(s)) return byteArray;
            byteArray = Encoding.UTF8.GetBytes(s);
            using (var stream = new MemoryStream())
            {
                using (var zip = new GZipStream(stream, CompressionMode.Compress))
                {
                    zip.Write(byteArray, 0, byteArray.Length);
                }
                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        internal static string Decompression(byte[] bytes)
        {
            if (bytes == null || bytes.Length <= 0) return string.Empty;
            using (var stream = new MemoryStream(bytes))
            {
                using (var zip = new GZipStream(stream, CompressionMode.Decompress))
                {
                    using (var reader = new StreamReader(zip))
                    {
                        return reader.ReadToEnd();
                    }

                }
            }


        }






        internal static DictionaryEntry<T, TD>[] DictionaryToArray<T, TD>(Dictionary<T, TD> dictionary)
        {
            var f = new List<DictionaryEntry<T, TD>>();
            dictionary.Keys.ToList().ForEach(a => f.Add(new DictionaryEntry<T, TD> { Key = a, Value = dictionary[a] }));
            return f.ToArray();
        }
        public static List<T> CollectionToList<T>(this ICollection other)
        {
            var output = new List<T>(other.Count);

            output.AddRange(other.Cast<T>());

            return output;
        }


    }
    [Serializable]
    public struct DictionaryEntry<T, TD>
    {
        public T Key;
        public TD Value;
    }
}
