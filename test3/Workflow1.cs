﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Schema;

namespace test3
{
    internal partial class Workflow
    {
        /// <summary>
        /// сериализация объекта в json 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public string Serialize(object obj)
        {
            if (obj.GetType().IsClass)
            {
                TypeDescriptor.AddAttributes(obj, new FirstObjectAttribute());
            }
            var res1 = SeralizeCore(obj, true);


            var sb = new StringBuilder(String.Format("{{\"{0}\":[", ConstString.TypeList));
            foreach (var v in _collection.TypeDictionary.Values)
            {
                sb.AppendFormat("\"{0}\"", v);
                sb.Append(",");
            }
            var res = sb.ToString().TrimEnd(',') + "]," + res1.TrimStart('{');
            return res;
        }
        public string SeralizeCore(object obj, bool addheadbody)
        {
            if (obj == null) return ConstString.NULL;
            var typer = obj.GetType();
            var sb = new StringBuilder("{");

            AddKeyTypeToDictionary(sb, typer, addheadbody);



            if (typer.IsEnum)
            {

                sb.AppendFormat("\"{0}\":\"{1}\"", ConstString.Single, obj);
                sb.Append("}");
                return sb.ToString();

            }
            if (typer.IsNumeric())
            {
                sb.AppendFormat("\"{0}\":{1}", ConstString.Single, obj);
                sb.Append("}");
                return sb.ToString();
            }

            if (obj is string || obj is char)
            {
                sb.AppendFormat("\"{0}\":\"{1}\"", ConstString.Single, obj.ToString().Replace("\"", "\\\""));
                sb.Append("}");
                return sb.ToString();
            }


            if (typer.GetMethodParse() != null)// && obj.GetType().IsSerializable
            {
                sb.AppendFormat("\"{0}\":\"{1}\"", ConstString.Single, obj);
                sb.Append("}");
                return sb.ToString();
            }

            if (typer.IsCollection())
            {


                FactoryValueSerialize.SerializeEnumerator(typer, sb, "", this, obj, ConstString.Single);
                sb.Append("}");
                return sb.ToString();

            }



            if (typer.IsEnumerable())
            {
                FactoryValueSerialize.SerializeEnumerator(typer, sb, "", this, obj, ConstString.Single);
                sb.Append("}");
                return sb.ToString();
            }

            if (!typer.IsSerializable) throw new SerializationException(typer.FullName + " Не потдерживает сериализацию.");

            var field = typer.GetFields(Helper.BindingFlags).Where(a => a.IsNotSerialized == false).ToList();

            foreach (var fieldInfo in field)
            {
                var last = field.Last() == fieldInfo ? "" : ",";
                var currenttypeadd = GetTypeSerializeTranslate(typer, fieldInfo);
                if (currenttypeadd != null)
                {
                    sb.AppendFormat("\"{0}\":\"{1}\"{2}", fieldInfo.Name, currenttypeadd.GetMethod("Serialize").Invoke(obj, new[] { fieldInfo.GetValueCore(obj) }), last);
                    continue;
                }

                if (fieldInfo.IsNotSerialized) continue;
                if (fieldInfo.FieldType.IsEnumerable() && fieldInfo.FieldType != typeof(string))
                {
                    var vv = fieldInfo.GetValueCore(obj);
                    FactoryValueSerialize.SerializeEnumerator(fieldInfo.FieldType, sb, last, this, vv, fieldInfo.Name);
                    continue;
                }

                sb.AppendFormat("\"{0}\":{1}{2}", fieldInfo.Name, FactoryValueSerialize.GetValue(fieldInfo, obj, this),
                    last);
            }


            return sb.ToString().TrimEnd(',') + "}";
        }

        object GetSingle(Object o, Type type)
        {
            if (type == typeof(string))
            {
                return o.ToString();
            }

            if (o is string && type.IsEnum)
            {
                return FactoryValueSerialize.GetValueParse(o, type);
            }

            if (type.GetMethodParse() != null)
            {
                return FactoryValueSerialize.GetValueParse(o, type);
            }
            if (type.IsEnumerable())
            {
                return GetValueEnumerable(o, type);
            }
            throw new SerializationException("Не могу ((" + type.FullName);
        }




        public object GetValueEnumerable(object obj, Type type)
        {

            // if (obj.ToString() == "null") return null;

            if (type.IsList())
            {
                var sd = Activator.CreateInstance(type);
                var dict = (ArrayList)obj;
                foreach (var v in dict)
                {
                    ((IList)sd).Add(DesirealizeOne(v, type.GetGenericArguments()[0]));

                }
                return sd;
            }


            if (type.IsDictionary())
            {
                var keys = ((IDictionary)obj).Keys.OfType<string>().ToArray();
                var basedict = Activator.CreateInstance(type);
                if (keys[0] == ConstString.Dictionary + ConstString.Single) //значит приведение словаря к списку
                {
                    return DeserializeDictionary.DeserialiseDictionarySingle(type, obj, keys[0], this);
                }
                var dict = (IDictionary)obj;
                foreach (var item in dict)
                {
                    var key = ((DictionaryEntry)item).Key;
                    var value = ((DictionaryEntry)item).Value;
                    var keycore = FactoryValueSerialize.GetValueParse(key, type.GetGenericArguments()[0]);
                    var valuecore = DesirealizeOne(value, type.GetGenericArguments()[1]);

                    ((IDictionary)basedict).Add(keycore, valuecore);
                }

                return basedict;

            }


         
            if (type.IsCollection() && type.GetElementType() == null)
            {
                var res = ((ICollection)obj).CollectionToList<Object>();
                var al = new ArrayList();
                var reslist = res.Select(re => DesirealizeOne(re, re.GetType()));
                var enumerable = reslist as object[] ?? reslist.ToArray();
                if (type == typeof(Stack) || type == typeof(ConcurrentBag<>))
                {
                    foreach (var source in enumerable.Reverse())
                        al.Add(source);
                }
                else
                {
                    foreach (var res1 in enumerable)
                        al.Add(res1);
                }



                if (type.GetConstructors().Where(a => a.GetParameters().Count() == 1).Any(a => a.GetParameters()[0].ParameterType == typeof(IDictionary)))
                {
                    var resd = al.Cast<object>().ToDictionary(d => ((DictionaryEntry)d).Key, d => ((DictionaryEntry)d).Value);
                    return Activator.CreateInstance(type, resd);
                }

                var r = Activator.CreateInstance(type, al);//1 параметр массив
                return r;



            }
            if (type.IsArray)
            {

                var sd = new ArrayList();
                foreach (var v in (IEnumerable)obj)
                {
                    sd.Add(DesirealizeOne(v, type.GetElementType()));
                }
                var ee = type.GetElementType();
                return sd.ToArray(ee);
            }
            if (type.IsSet())
            {

                dynamic cleanHashSet = Activator.CreateInstance(type);
                var dict = (ArrayList)obj;

                foreach (var v in dict)
                {
                    cleanHashSet.Add((dynamic)FactoryValueSerialize.GetValueParse(v, type.GetGenericArguments()[0]));
                }
                return cleanHashSet;
            }


            throw new SerializationException("Не могу ((" + type.FullName);
        }
        /// <summary>
        /// Заполнение названием типа, для передачи в json
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="type"></param>
        /// <param name="addheadbody"></param>
        void AddKeyTypeToDictionary(StringBuilder sb, Type type, bool addheadbody)
        {

            if (!addheadbody) return;
            if (type.AssemblyQualifiedName == null) return;
            var hash = type.AssemblyQualifiedName.GetHashCode();

            if (!_collection.TypeDictionary.ContainsKey(hash))
            {
                _collection.TypeDictionary.Add(hash, type.IsPrimitive ? type.FullName : type.AssemblyQualifiedName);
            }
            var index = _collection.TypeDictionary.Keys.TakeWhile(k => k != hash).Count();
            sb.AppendFormat("\"{1}\":\"{0}\",", index, String.Format(ConstString.Head, index));
        }

        static Type GetTypeSerializeTranslate(Type typer, FieldInfo fieldInfo)
        {
            var tt = typer.GetInterfaces().Where(a => a.IsGenericType && a.GetGenericTypeDefinition() == typeof(ISerializeTranslate<>));
            var enumerable = tt as Type[] ?? tt.ToArray();
            return enumerable.SingleOrDefault(a => a.GetGenericArguments().First() == fieldInfo.FieldType);
        }




    }
}
