﻿namespace test3
{
    /// <summary>
    /// Если тип не потдерживает сериализацию  чего то, или хочет изменить сериализацию чегото, то тип наследуем от этого интерфейса, 
    /// где T - тип чего то.( смотри примеры), этот интерфейс перекрывает базовую реализацию, если она реализована по дефолту
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISerializeTranslate<T>
    {
        string Serialize(T t);
        T Deserialize(string s);
    }
}