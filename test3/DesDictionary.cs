﻿using System;
using System.Collections;
using System.Reflection;

namespace test3
{
    internal static class DeserializeDictionary
    {

        public static Object DeserialiseDictionarySingle(Type dicType, object obj, string keys, Workflow workflow)
        {
            dynamic basedict = Activator.CreateInstance(dicType);
            var li = ((IDictionary)((IDictionary)obj)[keys])[ConstString.Single];
            foreach (var v in (IList)li)
            {
                var tkey = dicType.GetGenericArguments()[0];
                var tvalue = dicType.GetGenericArguments()[1];

                var key = ((IDictionary)v)["Key"];
                var value = ((IDictionary)v)["Value"];
                var k = workflow.DesirealizeOne(key, tkey);
                var vv = workflow.DesirealizeOne(value, tvalue);
                basedict.Add(k, vv);
            }
            return basedict;
        }
        public static Object DeserialiseDictionaryMoo(FieldInfo fieldInfo, object obj, Workflow workflow)
        {
            var basedict = (IDictionary)Activator.CreateInstance(fieldInfo.FieldType);
            var ee = ((IDictionary)obj)[ConstString.Single];
            foreach (var v in (IEnumerable)ee)
            {

                var tkey = fieldInfo.FieldType.GetGenericArguments()[0];
                var tvalue = fieldInfo.FieldType.GetGenericArguments()[1];

                var key1 = ((IDictionary)v)["Key"];
                var value1 = ((IDictionary)v)["Value"];
                var k = workflow.DesirealizeOne(key1, tkey);
                var vv = workflow.DesirealizeOne(value1, tvalue);
                basedict.Add(k, vv);

            }
            return basedict;
        }
    }
}
