﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Script.Serialization;

namespace test3
{
    [AttributeUsageAttribute(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    internal class FirstObjectAttribute : Attribute
    {
        
    }

    internal partial class Workflow
    {

        readonly Collection _collection = new Collection();








       

        public object DesirealizeCore(string json)
        {
            var serializer = new JavaScriptSerializer { MaxJsonLength = Helper.MaxJsonLength };
            var dict = serializer.Deserialize<Dictionary<string, object>>(json);
            var typestr = ((ArrayList)dict.First().Value);
            foreach (var v in typestr)
            {
                _collection.TypelistD.Add(v.ToString());
            }
            return DesirealizeOne(dict, null);
        }





        internal object DesirealizeOne(object list, Type type)
        {

            if (list == null) return null;
            if (list.ToString() == "null") return null;
            if (list is string && type == typeof(object)) return list.ToString();
            if (list.GetType().IsPrimitive || list is string)
            {
                var eobj = FactoryValueSerialize.GetValueParse(list, type);
                if (eobj != null) return eobj;
            }


            var basetype = type;

            if (list.GetType().IsDictionary())
            {
                var dict = (IDictionary)list;
                var keys = ((IDictionary)list).Keys;
                foreach (var key in keys)
                {
                    string k = key.ToString();
                    if (k.IndexOf(ConstString.Headone, StringComparison.Ordinal) != -1)
                    {
                        var i = Int32.Parse(k.Replace(ConstString.Headone, String.Empty));
                        basetype = Type.GetType(_collection.TypelistD[i]);
                        break;
                    }
                }
                foreach (var key in keys)
                {
                    var k = key.ToString();
                    if (k == ConstString.Single)
                    {
                        return GetSingle(dict[ConstString.Single], basetype);

                    }

                }
                if (basetype != null && basetype.IsEnumerable())
                    return GetSingle(dict, basetype);
            }

            if (basetype == null)
            {
                throw new SerializationException("Не могу создать тип из строки");

            }

            if (basetype.IsNullable())
            {
                return FactoryValueSerialize.GetValueParse(list, type);
            }

            var isDictionary = basetype.IsDictionary();
            var isList = basetype.IsList();
            var isListset = basetype.IsSet();
            var isArray = basetype.IsArray;

            if (isDictionary || isList || isListset || isArray || basetype.IsCollection())
            {
                return GetValueEnumerable(list, basetype);
            }

            var objectbase = Activator.CreateInstance(basetype);



            var keysfield = ((IDictionary)list).Keys;
            foreach (var key in keysfield)
            {
                if (key.ToString() == ConstString.TypeList || key.ToString().IndexOf(ConstString.Headone, StringComparison.Ordinal) != -1) continue;


                var eee = ((IDictionary)list)[key];

                var fieldd = basetype.GetField(key.ToString(), Helper.BindingFlags);
                if (fieldd == null)
                {
                    throw new SerializationException("sddddddddddddddd");
                }

                if (eee == null)
                {
                    fieldd.SetValueCore(objectbase, null);
                    continue;

                }


                var currenttypeadd = GetTypeSerializeTranslate(basetype, fieldd);
                if (currenttypeadd != null)
                {
                    var res = currenttypeadd.GetMethod("Deserialize").Invoke(objectbase, new object[] { eee.ToString() });
                    fieldd.SetValueCore(objectbase, res);
                    continue;
                }


                if (eee.GetType().IsDictionary())
                {
                    var li = ((IDictionary)list)[key];
                    if (li.GetType().IsDictionary())
                    {
                        var lii = ((IDictionary)li)[String.Concat(ConstString.Dictionary, key)];
                        if (lii != null)
                        {
                            var item = DeserializeDictionary.DeserialiseDictionaryMoo(fieldd, lii, this);
                            fieldd.SetValueCore(objectbase, item);
                            continue;
                        }
                    }
                }




                var value = DesirealizeOne(eee, fieldd.FieldType);
                fieldd.SetValueCore(objectbase, value);

            }

            return objectbase;

        }



        
    }
}