﻿using System;
using System.Collections.Generic;

namespace test3
{
    internal class Collection
    {

        private readonly Dictionary<int, string> _typeDictionary = new Dictionary<int, string>();
        private readonly List<string> _typelistD = new List<string>();
        private readonly List<IAsyncResult> _listIAsyncResult = new List<IAsyncResult>();
        public Dictionary<int, string> TypeDictionary {get{return _typeDictionary;}}
        public List<string> TypelistD{get { return _typelistD;}}
        public List<IAsyncResult> AsyncResultsList{get { return _listIAsyncResult; }}

    }
}
