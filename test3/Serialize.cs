﻿using System;
using System.IO;

namespace test3
{
    public static class Serialize
    {
        /// <summary>
        /// Сериализует объект в массив байт
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] SerializeCore(object obj)
        {
            if (obj == null) throw new NullReferenceException("Вы пытаетесь сериализовать null..");

            var ss = new Workflow().Serialize(obj);
            var res = Helper.Compression(ss);
           
#if DEBUG
            var str = Helper.Decompression(res);
            File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "ssss.txt"), str);
            //byte[] array = Encoding.UTF8.GetBytes(str);
#endif
            
            return res;

        }

#if DEBUG
        public static UInt32 GetCountBytes(object o)
        {
            return  (uint) SerializeCore(o).Length;
        }

#endif

       

        /// <summary>
        /// Десиреализует  массив байт в указанный тип.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static T DeserializeCore<T>(byte[] bytes)
        {
            
            return (T) new Workflow().DesirealizeCore(Helper.Decompression(bytes));

        }

        /// <summary>
        /// Сериализация в json формат.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string SerializeToJson(object o)
        {
            return new Workflow().Serialize(o);
        }

        /// <summary>
        /// Десериализация из строки формата json в указаный тип.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T DeserializeFromJson<T>(string json)
        {
            if (string.IsNullOrWhiteSpace(json)) throw new NullReferenceException("Вы пытаетесь сериализовать null..");
            return (T)new Workflow().DesirealizeCore(json);
        }




    }
}
